# Une super calculatrice

## Contexte
Créer la plus belle calculatrice en JavaScript !  
Celle-ci devra respecter les users stories données dans le readme

### Users stories
#### Niveau 1
* En tant qu'utilisateur, je peux **cliquer sur les touches de la calculatrice**
* En tant qu'utilisateur, je peux **additionner deux nombres** via les boutons de la calculatrice
* En tant qu'utilisateur, je peux **multiplier deux nombres** via les boutons de la calculatrice
* En tant qu'utilisateur, je peux **soustraire deux nombres** via les boutons de la calculatrice
* En tant qu'utilisateur, je peux **diviser deux nombres** via les boutons de la calculatrice
* En tant qu'utiliateur, je peux **réinitialiser** le calcul en cours
* En tant qu'utiliateur, je peux **valider** le calcul en cours
* En tant qu'utiliateur, je peux **voir le résultat obtenu**

#### Niveau 2
* En tant qu'utilisateur, je peux **faire des calculs avec des nombres décimaux**
* En tant qu'utilisateur, je peux **faire plusieurs opérations** en même temps
* En tant qu'utilisateur, je peux **transformer une valeur en pourcentage**

#### Niveau 3
* En tant qu'utilisateur, je peux **utiliser la calculatrice sur un ordinateur, une tablette ou un smartphone**
* En tant qu'utilisateur, je peux **utiliser la calculatrice avec le clavier**
* En tant qu'application, je peux **afficher le dernier calcul réalisé**

## Périmètre

### Planning
* 11/12/18 PM : Production
* 12/12/18 PM : Production
* 13/12/18 PM : Production
* 17/12/18 PM : Correction

### Réalisation
En solo

### Livrables
* Un super chiffrage (temps de réalisation pour chaque partie)
* Un prototype **papier** contenant tous les éléments demandés des 3 niveaux
* Le code source du projet

#### Compétences travaillées
* 1 : Maquetter une application (niveau 1)
* 2 : Réaliser une interface utilisateur wev statique et adaptable (niveau 1)